from django import forms
from django.forms import ModelForm
from .models import ArticleModel


class ArticleForm(ModelForm):
    class Meta:
        model = ArticleModel
        fields = ['title', 'category', 'author', 'content']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Title"}),
            'category': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Category"}),
            'author': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Author"}),
            'content': forms.Textarea(attrs={'class': 'form-control', 'placeholder': "Whats on your mind?"}),
            }
        labels = {
            'title': '',
            'category': '',
            'author': '',
            'content': '',
        }